%mathpiper

Prefix("@@", 0); // I'm really not sure what number to put here

Procedure("``", ["body"])
{
    Local(toFlattenPositions, backBodyString);
    toFlattenPositions := PositionsPattern(body, '@@q_);
    
    body := Substitute('@@, '@) body;
    
    // Horrible workaround I'm sure there's a better way
    backBodyString := "` '(" + UnparseMathPiper(body) + ");";
    body := Eval(PipeFromString(backBodyString) ParseMathPiper());
    
    toFlattenPositions := Sort(toFlattenPositions, Lambda([lhs, rhs], 
        {
            Local(lhsList, rhsList);
            
            If (lhs =? "") { True; }
            Else If (rhs =? "") { False; }
            Else
            {
                lhsList := PositionStringToList(lhs);
                rhsList := PositionStringToList(rhs);
                
                If (Length(lhsList) !=? Length(rhsList)) { Length(lhsList) >? Length(rhsList); }
                Else
                {
                    PopBack(lhsList) >? PopBack(rhsList);
                }
            }
        }
    ));
    
    ForEach(position, toFlattenPositions) 
    {
        Local(list, parent, parentPosition, parentExpression, locationInParent);
        
        If (position =? "") 
        {
            ExceptionThrow("Invalid Argument",
                "Invalid argument for ``, @@ cannot be root.");
        }
        
        parentPosition := PositionStringToList(position);
        locationInParent := PopBack(parentPosition);
        parentPosition := ListToString(parentPosition, ",");
        
        list := PositionGet(body, position);
        parent := PositionGet(body, parentPosition);
        
        If (!? List?(list)) 
        {
            ExceptionThrow("Invalid Argument",
                "Invalid argument for @@, must be a list. " + 
                "At tree location: " + position);
        }
        
        parent := ProcedureToList(parent);
                                       
        Delete!(parent, locationInParent + 1);
       
        ForEach(item, list)
        {
            Insert!(parent, locationInParent+1, item);
            locationInParent +:= 1;
        }
        
        parent := ListToProcedure(parent);
        
        SubstitutePosition(body, parentPosition, 'True, parent);
    }
    
    Eval(body);
}

UnFence("``", 1);
HoldArgumentNumber("``", 1, 1);
Prefix("``", 60000); // or here


a := [3, 4];
b := [1, 7, 4, 23232323];

compoundExample := `` ' Add(@@b, Add(@@a, 2, 3, Add(@@b, 1, @@b)));
%/mathpiper

    %output,mpversion=".262",preserve="false"
      Result: Add(1,7,4,23232323,Add(3,4,2,3,Add(1,7,4,23232323,1,1,7,4,23232323)))
.   %/output


%mathpiper
a := [3, 4];
b := [1, 7, 4, 23232323];

Show(TreeView(' `` @@ (`` Add(b, @@a))));

compoundExample := `` @@ (`` Add(b, @@a));
%/mathpiper

    %error,mpversion=".262",preserve="false"
      Result: The name <``> is an operator that cannot be evaluated without operands.  Error in another fold on or before line offset 13 into the fold starting at index 12.
.   %/error

%mathpiper
trivialCounterExample := `` @@a;
%/mathpiper

    %error,mpversion=".262",preserve="false"
      Result:  Error: Invalid argument for ``, @@ cannot be root. Error in another fold on or before line offset 21 into the fold starting at index 12.
.   %/error

%mathpiper
code := '{ index := 0; While(index <=? 10) { Echo(index); index++; } index; }
replacement := SubstitutePosition(code, "", '{a___; c_;}, ' `` '{@@a; index + 1;});
%/mathpiper

    %output,mpversion=".262",preserve="false"
      Result: 
      {
        index := 0;
      
        While(index <=? 10)
        {
          Echo(index);
      
          index++;
        }
      
        index + 1;
      }
.   %/output

