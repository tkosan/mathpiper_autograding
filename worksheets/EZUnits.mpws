%mathpiper
?sizeConversions :=
[
    GA <- 1000000000*A,
    GC <- 1000000000*C,
    GF <- 1000000000*F,
    GH <- 1000000000*H,
    GHz <- 1000000000*Hz,
    GJ <- 1000000000*J,
    GK <- 1000000000*K,
    GN <- 1000000000*N,
    GOhm <- 1000000000*Ohm,
    GPa <- 1000000000*Pa,
    GS <- 1000000000*S,
    GT <- 1000000000*T,
    GV <- 1000000000*V,
    GW <- 1000000000*W,
    GWb <- 1000000000*Wb,
    Gg <- 1000000*kg,
    Gm <- 1000000000*m,
    Gmol <- 1000000000*mol,
    Gs <- 1000000000*s,
    
    MA <- 1000000*A,
    MC <- 1000000*C,
    MF <- 1000000*F,
    MH <- 1000000*H,
    MHz <- 1000000*Hz,
    MJ <- 1000000*J,
    MK <- 1000000*K,
    MN <- 1000000*N,
    MOhm <- 1000000*Ohm,
    MPa <- 1000000*Pa,
    MS <- 1000000*S,
    MT <- 1000000*T,
    MV <- 1000000*V,
    MW <- 1000000*W,
    MWb <- 1000000*Wb,
    Mg <- 1000*kg,
    metric_ton <- 1000*kg,
    Mm <- 1000000*m,
    Mmol <- 1000000*mol,
    Ms <- 1000000*s,
    
    kA <- 1000*A,
    kC <- 1000*C,
    kF <- 1000*F,
    kH <- 1000*H,
    kHz <- 1000*Hz,
    kJ <- 1000*J,
    kK <- 1000*K,
    kN <- 1000*N,
    kOhm <- 1000*Ohm,
    kPa <- 1000*Pa,
    kS <- 1000*S,
    kT <- 1000*T,
    kV <- 1000*V,
    kW <- 1000*W,
    kWb <- 1000*Wb,
    kilogram <- kg,
    kilometer <- 1000*m,
    km <- 1000*m,
    kmol <- 1000*mol,
    ks <- 1000*s,
    
    mA <- A/1000,
    mC <- C/1000,
    mF <- F/1000,
    mH <- H/1000,
    mHz <- Hz/1000,
    mJ <- J/1000,
    mK <- K/1000,
    mN <- N/1000,
    mOhm <- Ohm/1000,
    mPa <- Pa/1000,
    mS <- S/1000,
    mT <- T/1000,
    mV <- V/1000,
    mW <- W/1000,
    mWb <- Wb/1000,
    mg <- kg/1000000,
    ml <- l/1000,
    mm <- m/1000,
    mmol <- mol/1000,
    ms <- s/1000,
    
    microA <- A/1000000,
    microampere <- A/1000000,
    microC <- C/1000000,
    microcoulomb <- C/1000000,
    microF <- F/1000000,
    microfarad <- F/1000000,
    microH <- H/1000000,
    microhenry <- H/1000000,
    microHz <- Hz/1000000,
    microhertz <- Hz/1000000,
    microJ <- J/1000000,
    microjoule <- J/1000000,
    microK <- K/1000000,
    microkelvin <- K/1000000,
    microN <- N/1000000,
    micronewton <- N/1000000,
    microOhm <- Ohm/1000000,
    microPa <- Pa/1000000,
    micropascal <- Pa/1000000,
    microS <- S/1000000,
    microsievert <- S/1000000,
    microT <- T/1000000,
    microtesla <- T/1000000,
    microV <- V/1000000,
    microvolt <- V/1000000,
    microW <- W/1000000,
    microwatt <- W/1000000,
    microWb <- Wb/1000000,
    microweber <- Wb/1000000,
    microg <- g/1000000,
    microgram <- kg/1000000000,
    microm <- m/1000000,
    micrometer <- m/1000000,
    micron <- m/1000000,
    micron <- m/1000000,
    micromol <- mol/1000000,
    micromole <- mol/1000000,
    micros <- s/1000000,
    microsecond <- s/1000000,
    
    nA <- A/1000000000,
    nC <- C/1000000000,
    nF <- F/1000000000,
    nH <- H/1000000000,
    nHz <- Hz/1000000000,
    nJ <- J/1000000000,
    nK <- K/1000000000,
    nN <- N/1000000000,
    nOhm <- Ohm/1000000000,
    nPa <- Pa/1000000000,
    nS <- S/1000000000,
    nT <- T/1000000000,
    nV <- V/1000000000,
    nW <- W/1000000000,
    nWb <- Wb/1000000000,
    ng <- g/1000000000,
    nm <- m/1000000000,
    nmol <- mol/1000000000,
    ns <- s/1000000000,
    
    pA <- A/1000000000000,
    pC <- C/1000000000000,
    pF <- F/1000000000000,
    pH <- H/1000000000000,
    pHz <- Hz/1000000000000,
    pJ <- J/1000000000000,
    pK <- K/1000000000000,
    pN <- N/1000000000000,
    pOhm <- Ohm/1000000000000,
    pPa <- Pa/1000000000000,
    pS <- S/1000000000000,
    pT <- T/1000000000000,
    pV <- V/1000000000000,
    pW <- W/1000000000000,
    pWb <- Wb/1000000000000,
    pg <- g/1000000000000,
    pm <- m/1000000000000,
    pmol <- mol/1000000000000,
    ps <- s/1000000000000,
    
    fA <- A/1000000000000000,
    fC <- C/1000000000000000,
    fF <- F/1000000000000000,
    fH <- H/1000000000000000,
    fHz <- Hz/1000000000000000,
    fJ <- J/1000000000000000,
    fK <- K/1000000000000000,
    fN <- N/1000000000000000,
    fOhm <- Ohm/1000000000000000,
    fPa <- Pa/1000000000000000,
    fS <- S/1000000000000000,
    fT <- T/1000000000000000,
    fV <- V/1000000000000000,
    fW <- W/1000000000000000,
    fWb <- Wb/1000000000000000,
    fg <- g/1000000000000000,
    fm <- m/1000000000000000,
    fmol <- mol/1000000000000000,
    fs <- s/1000000000000000,
];

?standardConversions := 
[
    AU <- astronomical_unit,
    Bq <- becquerel,
    Btu <- 1055*J,
    C <- A*s,
    F <- C^2/J,
    Gy <- gray,
    H <- J/A^2,
    Hz <- 1/s,
    J <- N*m,
    N <- (kg*m)/s^2,
    Ohm <- (J*s)/C^2,
    Pa <- N/m^2,
    R <- (5*K)/9,
    S <- 1/Ohm,
    Sv <- sievert,
    T <- J/(A*m^2),
    V <- J/C,
    W <- J/s,
    Wb <- J/A,
    acre <- 4840*yard^2,
    amp <- A,
    ampere <- A,
    astronomical_unit <- 149597870700*m,
    becquerel <- 1/s,
    candela <- cd,
    cfm <- feet^3/minute,
    cm <- m/100,
    coulomb <- C,
    cup <- pint/2,
    day <- 86400*s,
    degree <- Pi/180,
    farad <- F,
    feet <- (381*m)/1250,
    fl_oz <- fluid_ounce,
    fluid_ounce <- cup/8,
    foot <- feet,
    ft <- feet,
    g <- kg/1000,
    gallon <- 231*inch^3,
    gill <- cup/2,
    gram <- g,
    gray <- J/kg,
    ha <- hectare,
    hectare <- 10000*m^2,
    henry <- H,
    hertz <- Hz,
    horsepower <- (550*foot*pound_force)/s,
    hour <- 3600*s,
    hp <- horsepower,
    inch <- feet/12,
    joule <- J,
    julian_year <- 31557600*s,
    kat <- katal,
    katal <- mol/s,
    kelvin <- K,
    l <- liter,
    lbf <- pound_force,
    lbm <- pound_mass,
    light_year <- (299792458*julian_year*m)/s,
    liter <- m^3/1000,
    lumen <- cd,
    lux <- lumen/m^2,
    meter <- m,
    mile <- 5280*feet,
    minute <- 60*s,
    mole <- mol,
    month <- 2629800*s,
    newton <- N,
    ohm <- Ohm,
    ounce <- pound_mass/16,
    oz <- ounce,
    parsec <- ((648000*astronomical_unit)/Pi),
    pascal <- Pa,
    pc <- parsec,
    pint <- quart/2,
    pound_force <- (196133*ft*pound_mass)/(6096*s^2),
    pound_mass <- (45359237*kg)/100000000,
    psi <- pound_force/inch^2,
    quart <- gallon/4,
    rod <- (33*feet)/2,
    second <- s,
    short_ton <- 2000*lbm,
    siemens <- S,
    sievert <- gray,
    slug <- (pound_force*s^2)/foot,
    tablespoon <- fluid_ounce/2,
    tbsp <- tablespoon,
    teaspoon <- tablespoon/3,
    tesla <- T,
    tsp <- teaspoon,
    volt <- V,
    watt <- W,
    weber <- Wb,
    week <- 604800*s,
    yard <- 3*feet,
    year <- 31557600*s,
];

?recognizedUnits := ['A, 'AU, 'Bq, 'Btu, 'C, 'F, 'GA, 'GC, 'GF, 'GH, 'GHz, 'GJ, 'GK, 'GN, 'GOhm, 'GPa, 'GS, 'GT, 'GV, 'GW, 'GWb, 'Gg, 'Gm, 'Gmol, 'Gs, 'Gy, 'H, 'Hz, 'J, 'K, 'MA, 'MC, 'MF, 'MH, 'MHz, 'MJ, 'MK, 'MN, 'MOhm, 'MPa, 'MS, 'MT, 'MV, 'MW, 'MWb, 'Mg, 'Mm, 'Mmol, 'Ms, 'N, 'Ohm, 'Pa, 'Pi, 'R, 'S, 'Sv, 'T, 'V, 'W, 'Wb, 'acre, 'amp, 'ampere, 'astronomical_unit, 'becquerel, 'candela, 'cd, 'cfm, 'cm, 'coulomb, 'cup, 'day, 'degree, 'fA, 'fC, 'fF, 'fH, 'fHz, 'fJ, 'fK, 'fN, 'fOhm, 'fPa, 'fS, 'fT, 'fV, 'fW, 'fWb, 'farad, 'feet, 'fg, 'fl_oz, 'fluid_ounce, 'fm, 'fmol, 'foot, 'fs, 'ft, 'g, 'gallon, 'gill, 'gram, 'gray, 'ha, 'hectare, 'henry, 'hertz, 'horsepower, 'hour, 'hp, 'inch, 'joule, 'julian_year, 'kA, 'kC, 'kF, 'kH, 'kHz, 'kJ, 'kK, 'kN, 'kOhm, 'kPa, 'kS, 'kT, 'kV, 'kW, 'kWb, 'kat, 'katal, 'kelvin, 'kg, 'kilogram, 'kilometer, 'km, 'kmol, 'ks, 'l, 'lbf, 'lbm, 'light_year, 'liter, 'lumen, 'lux, 'm, 'mA, 'mC, 'mF, 'mH, 'mHz, 'mJ, 'mK, 'mN, 'mOhm, 'mPa, 'mS, 'mT, 'mV, 'mW, 'mWb, 'meter, 'metric_ton, 'mg, 'microA, 'microC, 'microF, 'microH, 'microHz, 'microJ, 'microK, 'microN, 'microOhm, 'microPa, 'microS, 'microT, 'microV, 'microW, 'microWb, 'microampere, 'microcoulomb, 'microfarad, 'microg, 'microgram, 'microhenry, 'microhertz, 'microjoule, 'microkelvin, 'microm, 'micrometer, 'micromol, 'micromole, 'micron, 'micronewton, 'micropascal, 'micros, 'microsecond, 'microsievert, 'microtesla, 'microvolt, 'microwatt, 'microweber, 'mile, 'minute, 'ml, 'mm, 'mmol, 'mol, 'mole, 'month, 'ms, 'nA, 'nC, 'nF, 'nH, 'nHz, 'nJ, 'nK, 'nN, 'nOhm, 'nPa, 'nS, 'nT, 'nV, 'nW, 'nWb, 'newton, 'ng, 'nm, 'nmol, 'ns, 'ohm, 'ounce, 'oz, 'pA, 'pC, 'pF, 'pH, 'pHz, 'pJ, 'pK, 'pN, 'pOhm, 'pPa, 'pS, 'pT, 'pV, 'pW, 'pWb, 'parsec, 'pascal, 'pc, 'pg, 'pint, 'pm, 'pmol, 'pound_force, 'pound_mass, 'ps, 'psi, 'quart, 'rod, 's, 'second, 'short_ton, 'siemens, 'sievert, 'slug, 'tablespoon, 'tbsp, 'teaspoon, 'tesla, 'tsp, 'volt, 'watt, 'weber, 'week, 'yard, 'year];
?fundamentalDimensions := ['Length, 'Mass, 'Time, 'Current, 'Temperature, 'Quantity, 'LuminousIntensity];
?SIUnits               := ['m, 'kg, 's, 'A, 'K, 'mol, 'cd, 'meter, 'kilogram, 'second, 'amp, 'ampere, 'kelvin, 'mole, 'candela, 'lumen];
?fundamentalUnitConversions := [m <- 'Length, kg <- 'Mass, s <- 'Time, A <- 'Current, K <- 'Temperature, mol <- 'Quantity, cd <- 'LuminousIntensity];

ForEach(unit, ?recognizedUnits)
{
    Constant(unit, True);
}

RulebaseHoldArguments("~", ["lhs", "rhs"]);
Infix("~", 50); // so 1 ~ J/s =? 1 ~ (J/s)

RulebaseHoldArguments("~~", ["lhs", "rhs"]);
Infix("~~", 10000);

// define physical constants
{
    ?c      := 299792458 ~ m/s;
    ?mu?0   := 314159265358979323/250000000000000000000000 ~ N/A^2;
    ?e?0    := 62500000000000000000000/7058806667238419595678034789541443 ~ (A^2*s^2)/(N*m^2);
    ?Z?0    := 47091289182721331806672967/125000000000000000000000 ~ (N*m)/(A^2*s);
    ?G      := 166857/2500000000000000 ~ m^3/(kg*s^2);
    ?h      := 41412931/62500000000000000000000000000000000000000 ~ J*s;
    ?h?bar  := 41412931/(125000000000000000000000000000000000000000*Pi) ~ J*s;
    
    ?m?P    := (7*Sqrt(126686575280351))/(5000000000000*Sqrt(166857)*Sqrt(Pi))
                  ~ (Sqrt(J)*Sqrt(kg)*s)/m;
                  
    ?k      := 1.3806504E-23 ~ J/K;
    
    ?T?P    := (9.113510923775812E+27*Sqrt(126686575280351))/(Sqrt(166857)*Sqrt(Pi))
                  ~ (K*Sqrt(kg)*m)/(Sqrt(J)*s);
                  
    ?l?P    := (5916133*166857^(1/2)*126686575280351^-(1/2))
                 /(7494811450000000000000000000000000000*Sqrt(Pi))
                    ~ (Sqrt(J)*s)/Sqrt(kg); 
                    
    ?t?P    := (5916133*166857^(1/2)*126686575280351^-(1/2))
                 /(2246887946842044100000000000000000000000000000*Sqrt(Pi))
                  ~ (Sqrt(J)*s^2)/(Sqrt(kg)*m); 
                  
    ?e      := 1602176487/10000000000000000000000000000 ~ C;
    ?Phi?0  := 2.067833667E-15 ~ Wb;
    ?G?0    := 7.7480917004E-5 ~ S;
    ?K?J    := 4.83597891E+14 ~ Hz/V;
    ?R?K    := 25812.807557 ~ ohm;
    ?mu?B   := 9.27400915E-24 ~ J/T;
    ?mu?N   := 5.05078324E-27 ~ J/T;
    ?alpha  := 0.0072973525376;
    ?R?inf  := 1.0973731568527E+7 ~ 1/m;
    ?a?0    := 5.2917720859E-11 ~ m;
    ?E?h    := 4.35974394E-18 ~ J;
    ?ratio?h?me := 3.6369475199E-4 ~ m^2/s;
    ?m?e    := 9.10938215E-31 ~ kg;
    ?N?A    := 602214179000000000000000 ~ 1/mol;
    ?m?u    := 1.660538782E-27 ~ kg;
    ?F      := 96485.3399 ~ C/mol;
    ?R      := 1039309/125000 ~ J/(K*mol);
    ?V?m    := 0.022413996 ~ m^3/mol;
    ?n?0    := 2.6867774E+25 ~ 1/m^3;
    ?ratio?S0?R := -1.1648677;
    ?sigma  := 5.6704E-8 ~ W/(K^4*m^2);
    ?c?1    := 3.74177118E-16 ~ W*m^2;
    // ?c?1L   := 1.191042759E-16 ~ (W*m^2)/sr; couldn't find what sr was
    ?c?2    := 0.014387752 ~ K*m;
    ?b      := 0.0028977685 ~ K*m;
    ?b?prime:= 5.878933E+10 ~ Hz/K; 
}

5  # a_Number? ~ b_Number? <-- a * b;
10 # (a_~u_) + (b_~u_) <-- (a + b)~u;
11 # (a_~u_) - (b_~u_) <-- (a - b)~u;
12 # (a_~u_) * (b_~v_) <-- SimplifyUnits((a * b)~(u * v));  
13 # (a_~u_) / (b_~v_) <-- SimplifyUnits((a / b)~(u / v));

15 # (a_ ~ u_) ~ v_ <-- SimplifyUnits(a ~ (u * v));
16 # a_ ~ (u_ ~ v_) <-- SimplifyUnits(a ~ (u * v));

22 # a_ ~ (s_Number? * u_) <-- s * (a ~ u);
23 # s_Number? * (a_~u_) <-- (s * a) ~ u;
24 # a_ ~ (u_ / s_Number?) <-- (a ~ u) / s;
25 # (a_~u_) / s_Number? <-- (a / s) ~ u;
26 # s_Number? / (a_ ~ u_) <-- (s / a) ~ (u ^ -1);
30 # (a_~u_) ^ b_Number? <-- (a ^ b) ~ (u ^ b);
40 # a_~1 <-- a;

50 # a_ ~ ( (s_Number? * u_) / (t_Number? * v_) ) <-- (a * (s / t)) ~ (u / v);
55 # a_ ~ ( (s_Number? * u_) / (v_) ) <-- (a * (s / 1)) ~ (u / v);
56 # a_ ~ ( (u_) / (t_Number? * v_) ) <-- (a * (1 / t)) ~ (u / v);

10 # ValueOf(a_ ~ u_) <-- a;
15 # ValueOf(a_Number?) <-- a;
20 # ValueOf(u_CharacterSymbol?) <-- 1;

10 # UnitOf(a_ ~ u_) <-- u;
15 # UnitOf(a_Number?) <-- 1;
20 # UnitOf(u_) <-- u;

40 # (a_~u_ + b_~v_) ~~ w_ <-- (a~u ~~ w) + (b~v ~~ w);
41 # (a_~u_ - b_~v_) ~~ w_ <-- (a~u ~~ w) - (b~v ~~ w);
50 # a_ ~~ b_ <-- ConvertUnits(a, b);
%/mathpiper

    %output,mpversion=".264",preserve="false"
      Result: True
.   %/output

%mathpiper


RecognizedUnit?(unit) := Contains?(?recognizedUnits, unit);
SIUnit?(unit) := Contains?(?SIUnits, unit);
NotSIUnit?(unit) := CharacterSymbol?(unit) &? Not?(SIUnit?(unit));

CharacterSymbol?(expression) :=
{
    Atom?(expression) &? StringMatches?(ToString(expression), "[a-zA-Z_]+");
}

PureUnitsToStandardUnits(expression) :=
{
    Local(unitPositions);
    
    unitPositions := PositionsPattern2(expression, u_CharacterSymbol?);
    unitPositions := Sort(unitPositions, Lambda([lhs, rhs], 
        {
            Local(lhsList, rhsList);
            
            If (lhs =? "") { True; }
            Else If (rhs =? "") { False; }
            Else
            {
                lhsList := PositionStringToList(lhs);
                rhsList := PositionStringToList(rhs);
                
                If (Length(lhsList) !=? Length(rhsList)) { Length(lhsList) >? Length(rhsList); }
                Else
                {
                    PopBack(lhsList) >? PopBack(rhsList);
                }
            }
        }
    ));
    
    ForEach(position, unitPositions)
    {
        expression := SubstitutePosition(expression, position, u_, '(1~ (` ' '@u)));
    }
    
    Eval(expression); // Why is this necessary, to trigger reduction
}

UnitsToFundamentalForm(expression) :=
{
    Local(cachedValue);
    
    cachedValue := ?unitsToFundamentalFormCache[expression];
    
    If (cachedValue =? None)
    {
        expression := expression /: ?sizeConversions;
        expression /:: ?standardConversions;
    }
    Else
    {
        cachedValue; // Only putting simple units in the cache
    }
}

If (Not?(Assigned?(?unitsToFundamentalFormCache)))
{
    ?unitsToFundamentalFormCache := [];
    
    ForEach(unit, ?recognizedUnits)
    {
        ?unitsToFundamentalFormCache[unit] := UnitsToFundamentalForm(unit);
    }
}

ConvertUnits(expression, toUnit) := 
{
    Local(value, unit, elementaryUnit, elementaryToUnit, newValue);
    
    If (DimensionsAsListOf(expression) !=? DimensionsAsListOf(toUnit))
    {
        ExceptionThrow("Invalid Conversion", 
            "Invalid Conversion: the left side has dimensions: <" + DimensionsOf(expression) +
            "> While the right side has different dimensions: <" + DimensionsOf(toUnit) + ">");
    }
    
    value := ValueOf(expression);
    unit  := UnitOf(expression);
    
    elementaryUnit   := UnitsToFundamentalForm(unit);
    elementaryToUnit := UnitsToFundamentalForm(toUnit);  
    
    newValue := ((value * (elementaryUnit)) / (elementaryToUnit));
    
    newValue := Solve(_reducedForm == newValue, _reducedForm)[1][2]; // Workaround for reduced form
    
    newValue ~ toUnit;
}

// Error Checking to avoid infinite loop possibilities
DeclareUnitConversion(conversion) :=
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
    
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot redefine a built-in unit");
        
    Append!(?standardConversions, conversion);
}

RemoveUnitConversion(conversion) :=
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
    
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot undefine a built-in unit");
        
    ?standardConversions := Remove(?standardConversions, conversion);
}

DeclareDimensionConversion(conversion) := 
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
        
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot redeclare a built-in unit");
        
    Append!(?fundamentalUnitConversions, conversion);
}

RemoveDimensionConversion(conversion) := 
{
    If (Atom?(conversion) |? conversion[0] !=? '<-)
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: input must be a <- conversion");

    If (!? Atom?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: the left side of a conversion must be a simple unit.");
        
    If (RecognizedUnit?(conversion[1]))
        ExceptionThrow("Invalid Conversion", "Invalid Conversion: cannot remove a built-in unit");
        
   ?fundamentalUnitConversions := Remove(?fundamentalUnitConversions, conversion);
}

DeclareFundamentalDimension(dimension) :=
{
    Append!(?fundamentalDimensions, dimension);
}

RemoveFundamentalDimension(dimension) :=
{
    ?fundamentalDimensions := Remove(?fundamentalDimensions, dimension);
}

FundamentalUnitsOf(unit) :=
{
    Local(fundamentalUnits, unitPositions);
    
    unit := UnitOf(unit);

    fundamentalUnits := UnitsToFundamentalForm(unit);
    
    fundamentalUnits := PureUnitsToStandardUnits(fundamentalUnits);
    
    fundamentalUnits := UnitOf(fundamentalUnits);
    
    Solve(_reducedForm == fundamentalUnits, _reducedForm)[1][2]; // Workaround for reduced form
}

DimensionsOf(expression) :=
{
    Local(fundamentalUnits);
    
    fundamentalUnits := FundamentalUnitsOf(expression);
    
    fundamentalUnits /: ?fundamentalUnitConversions;
}

DimensionsAsListOf(expression) :=
{
    Local(fundamentalDimensionsList, fundamentalDimensionsExpression, fundamentalDenominator, dimensionLocation, dimensionExponent);

    fundamentalDimensionsList := [];
    fundamentalDimensionsExpression := DimensionsOf(expression);
    fundamentalDenominator := Denominator(fundamentalDimensionsExpression);
    
    If (OccurrencesCount(fundamentalDenominator, 'Denominator) !=? 0)
    {
        fundamentalDenominator := 1; // Workaround for Denominator(m) =? Denominator(m)
    }
    
    ForEach(dimension, ?fundamentalDimensions)
    {    
        dimensionLocation := PositionsPattern(fundamentalDimensionsExpression, dimension^n_);
        
        If (Length(dimensionLocation) =? 0) 
        {
            dimensionLocation := PositionsPattern(fundamentalDimensionsExpression, dimension);
            
            If (Length(dimensionLocation) =? 1)
            {
                Append!(fundamentalDimensionsList, 1);
            }
            Else If(Length(dimensionLocation) !=? 0)
            {
                ExceptionThrow("Invalid Expression", "Bad fundamental dimensions: " + fundamentalDimensionsExpression); 
            }
            Else
            {
                Append!(fundamentalDimensionsList, 0);
            }
        }
        Else If (Length(dimensionLocation) =? 1)
        {
            dimensionLocation := dimensionLocation[1];
            dimensionExponent := PositionGet(fundamentalDimensionsExpression, dimensionLocation)[2];
            
            Append!(fundamentalDimensionsList, dimensionExponent);
        }
        Else
        {
            ExceptionThrow("Invalid Expression", "Bad fundamental dimensions: " + fundamentalDimensionsExpression);
        }
        
        If (OccurrencesCount(fundamentalDenominator, dimension) !=? 0)
        {
            Local(current);
            
            current := PopBack(fundamentalDimensionsList);
            current := current * -1;
            Append!(fundamentalDimensionsList, current);
        }
    }
    
    fundamentalDimensionsList;
}

SimplifyUnits(expression) :=
{
    Local(elementaryUnit, exUnit, exValue, fundamentalUnit, largestUnit, replacementUnit, returnValue, unit, unitPositions, units, unitsOfDimension, unitsOfDimensionWithSize);
    
    If (Not?(Assigned?(?avoidNestedSimplify)))
    {
        ?avoidNestedSimplify := True; 
        returnValue := expression;
    
        exValue := ValueOf(expression);
        exUnit  := UnitOf(expression);
        
        If (PositionsPattern2(exUnit, u_SIUnit?) !=? [] )
        {
            If (PositionsPattern2(exUnit, u_NotSIUnit?) !=? []) // only convert if there are mixed SI / non-SI units
            {
                elementaryUnit := UnitsToFundamentalForm(exUnit);
                elementaryUnit := Solve(_reducedForm == elementaryUnit, _reducedForm)[1][2]; // Workaround for reduced form
                
                elementaryUnit := PureUnitsToStandardUnits(elementaryUnit);
                
                returnValue := Eval(exValue ~ elementaryUnit);
            }
            Else
            {
                returnValue := expression;
            }
        }
        Else
        {
            unitPositions := PositionsPattern2(exUnit, u_CharacterSymbol?);
            units := [];
            
            ForEach(position, unitPositions)
                Append!(units, PositionGet(exUnit, position));
            
            ForEach(unit, units)
                exUnit := exUnit /: [ListToProcedure(['<-, unit, 1~unit])]; // Important for recombining replaced units
                
            ForEach(dimension, ?fundamentalDimensions)
            {
                unitsOfDimension := [];
                ForEach(unit, units) 
                    If (DimensionsOf(unit) =? dimension)
                        Append!(unitsOfDimension, unit);
                        
                unitsOfDimension := RemoveDuplicates(unitsOfDimension);
                
                If (Length(unitsOfDimension) >? 1)
                {
                    //Echo(dimension + ", " + unitsOfDimension);
                
                    unitsOfDimensionWithSize := [];
                    fundamentalUnit := FundamentalUnitsOf(unitsOfDimension[1]); // this will be the same for all unitsOfDimension
                    ForEach(unit, unitsOfDimension)
                        Append!(unitsOfDimensionWithSize,
                            [unit, ValueOf(1 ~ unit ~~ fundamentalUnit)]);
                            
                    unitsOfDimensionWithSize := Sort(unitsOfDimensionWithSize, Lambda([x,y], x[2] >? y[2]));
                    largestUnit := unitsOfDimensionWithSize[1][1];
                    
                    // Important to replace the largest unit first;
                    //Echo(exUnit);
                    ForEach(unitSize, unitsOfDimensionWithSize)
                    {
                        unit := unitSize[1];
                        replacementUnit := 1~unit ~~ largestUnit;
                        exUnit := exUnit /: [ListToProcedure(['<-, unit, replacementUnit])]; // ListToProcedure because <- holds its arguments
                        //Echo(exUnit);
                    }
                }
            }
            //Echo(units);                   
            
            returnValue := Eval(exValue ~ exUnit);
        }
        
        Unassign(?avoidNestedSimplify);
        returnValue;
    }
    Else
    {
        expression;
    }
}

SimplifyUnits(1~m);

%/mathpiper

    %output,mpversion=".264",preserve="false"
      Result: 1 ~ m
.   %/output

