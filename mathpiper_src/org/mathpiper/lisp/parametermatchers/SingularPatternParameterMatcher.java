/* {{{ License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */ //}}}
// :indentSize=4:lineSeparator=\n:noTabs=false:tabSize=4:folding=explicit:collapseFolds=0:

package org.mathpiper.lisp.parametermatchers;

import org.mathpiper.lisp.Environment;
import org.mathpiper.lisp.LispError;
import org.mathpiper.lisp.cons.Cons;

public abstract class SingularPatternParameterMatcher extends PatternParameterMatcher {/**
     *Check whether some expression matches to the pattern.
     *@aEnvironment the underlying Lisp environment.
     *@aExpression the expression to test.
     *@arguments (input/output) actual values of the pattern variables for aExpression.
     */ 
    
    protected abstract boolean doMatch(Environment aEnvironment, int aStackTop, Cons aExpressionTree, Cons[] arguments) throws Throwable;

    @Override
    public boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons aExpressionTree, Cons[] arguments) throws Throwable {
        if (aExpressionTree == null) {
            return false;
        }
        
        boolean match = doMatch(aEnvironment, aStackTop, aExpressionTree, arguments);
        
        return match
                && getNextMatcher().argumentMatches(aEnvironment, aStackTop, aExpressionTree.cdr(), arguments);
    }
    
    @Override
    public boolean argumentMatches(Environment aEnvironment, int aStackTop, Cons[] aExpressionTree, int aTreeLocation, Cons[] arguments) throws Throwable {
        if (aExpressionTree == null) {
            return false;
        }
        
        if (aTreeLocation >= aExpressionTree.length) {
            LispError.throwError(aEnvironment, aStackTop, "Listed function definitions need at least two parameters.");
        }
        
        boolean match = doMatch(aEnvironment, aStackTop, aExpressionTree[aTreeLocation], arguments);
        
        return match
                && getNextMatcher().argumentMatches(aEnvironment, aStackTop, aExpressionTree, aTreeLocation+1, arguments);
    }
}
